package com.puter.show

import android.util.Log
import junit.framework.Assert.assertEquals
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test

internal class CalculatorTest {

    companion object {
        @JvmStatic
        @BeforeClass
        fun beforeClass() {
            Log.i("TEST", "beforeClass")
        }

        @JvmStatic
        @AfterClass
        fun afterClass() {
            Log.i("TEST", "afterClass")
        }
    }

    @Test
    fun sum() {
        Log.i("TEST", "sum")
        assertEquals(32, Calculator.sum(16, 16))
    }

    @Test
    fun div() {
        Log.i("TEST", "div")
        assertEquals(22, Calculator.div(66, 3))
    }

    @Test
    fun mul() {
        Log.i("TEST", "mul")
        assertEquals(15, Calculator.mul(5, 3))
    }

    @Test
    fun square(){
        Log.i("TEST","square")
        assertEquals(100,Calculator.square(10))
    }
}